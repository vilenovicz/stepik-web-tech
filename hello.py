def wsgi_app(env, start_resp):
    status = '200 OK'
    headers = [
	('Content-Type','text/plain')
    ]
    body = [bytes(i + '\n') for i in env['QUERY_STRING'].split('&')]
    start_resp(status, headers)
    return body